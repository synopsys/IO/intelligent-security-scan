# Changelog

## 0.1.2
- Add dynamic Bitbucket branch feature

## 0.1.1
- Add slack integration

## 0.1.0

- Use of Intelligence Orchastration to determine which activity to perform.
- Surface SAST and SCA scanning information to pull request page, so that author and reviewers are able to make better informed decisions. The information comprises two parts; annotations and reports.
